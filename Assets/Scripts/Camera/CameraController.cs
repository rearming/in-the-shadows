﻿using UnityEngine;

namespace Camera
{
	public class CameraController : MonoBehaviour, ICameraProvider, IObjectTracer
	{
		public UnityEngine.Camera Camera { get; private set; }

		private void Awake()
		{
			Camera = UnityEngine.Camera.main;
		}

		public bool TraceObject(out RaycastHit hit)
		{
			return Physics.Raycast(Camera.ScreenPointToRay(Input.mousePosition), out hit);
		}
	}
}