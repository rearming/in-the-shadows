﻿namespace Camera
{
	public interface ICameraProvider
	{
		UnityEngine.Camera Camera { get; }
	}
}