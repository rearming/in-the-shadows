﻿using UnityEngine;

namespace Camera
{
	public interface IObjectTracer
	{
		bool TraceObject(out RaycastHit hit);
	}
}