using Camera;
using Game;
using GameSettings;
using GameSettings.ScriptableObjects;
using Object;
using Object.ScriptableObjects;
using Sirenix.Utilities;
using UnityEngine;
using Zenject;

namespace DI
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private MeshObject meshObjectPrefab;
        [SerializeField] private InputSettings inputSettings;
        [SerializeField] private ObjectManipulationSettings objectManipulationSettings;
        [SerializeField] private ObjectValidationSettings objectValidationSettings;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<CameraController>().FromComponentsInHierarchy().AsSingle();
            Container.BindInterfacesAndSelfTo<DifficultyManager>().FromComponentsInHierarchy().AsSingle();
            Container.BindInterfacesAndSelfTo<ObjectManager>().FromComponentsInHierarchy().AsSingle();
            Container.BindInterfacesAndSelfTo<LevelManager>().FromComponentsInHierarchy().AsSingle();
            Container.BindInterfacesAndSelfTo<WinManager>().FromComponentsInHierarchy().AsSingle();

            Container.Bind<MeshObject>().FromInstance(meshObjectPrefab).AsSingle();
            Container.Bind<InputSettings>().FromInstance(inputSettings).AsSingle();
            Container.Bind<ObjectManipulationSettings>().FromInstance(objectManipulationSettings).AsSingle();
            Container.Bind<ObjectValidationSettings>().FromInstance(objectValidationSettings).AsSingle();

            FindObjectsOfType<MeshObjectSpec>().ForEach(spec =>
            {
                Debug.Log($"[{spec.name}] injection!");
                Container.QueueForInject(spec);
            });
        }
    }
}