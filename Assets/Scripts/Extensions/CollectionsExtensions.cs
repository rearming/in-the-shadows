﻿using System.Collections.Generic;
using System.Linq;

namespace Extensions
{
	public static class CollectionsExtensions
	{
		public static T Random<T>(this IEnumerable<T> collection)
		{
			if (!(collection is IList<T> list))
				list = collection.ToList();
			return list[UnityEngine.Random.Range(0, list.Count)];
		}
	}
}
