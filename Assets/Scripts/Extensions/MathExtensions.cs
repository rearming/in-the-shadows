﻿using UnityEngine;

namespace Extensions
{
	public static class MathExtensions
	{
		public static Vector3 Div(this in Vector3 a, in Vector3 b) => new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
		public static float Max(this in Vector3 v) => Mathf.Max(Mathf.Max(v.x, v.y), v.z);
		public static Vector2 Swap(this in Vector2 v) => new Vector2(v.y, v.x);

		public static Vector3 SetX(this in Vector3 v, float x) => new Vector3(x, v.y, v.z);
		public static Vector3 SetY(this in Vector3 v, float y) => new Vector3(v.x, y, v.z);
		public static Vector3 SetZ(this in Vector3 v, float z) => new Vector3(v.x, v.y, z);
		
		public static Vector3 ClampAngleTo180(this Vector3 angle)
		{
			while (angle.x > 180f)
				angle.x -= 360f;
			while (angle.y > 180f)
				angle.y -= 360f;
			while (angle.z > 180f)
				angle.z -= 360f;
			return angle;
		}

		public static float Abs(this float f) => Mathf.Abs(f);
		
		public static Quaternion ToQuaternion(this Vector3 eulerAngles) => Quaternion.Euler(eulerAngles);
	}
}