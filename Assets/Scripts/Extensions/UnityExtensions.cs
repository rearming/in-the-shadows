﻿using UnityEngine;

namespace Extensions
{
	public static class UnityExtensions
	{
		public static void DisableRaycast(this GameObject gameObject) => gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");

		public static Color SetA(this in Color color, float a) => new Color(color.r, color.g, color.b, a);
	}
}