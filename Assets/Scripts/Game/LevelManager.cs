using System;
using System.Collections.Generic;
using GameSettings;
using Object;
using Sirenix.Serialization;
using UnityEngine;
using Zenject;

namespace Game
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private List<LevelSpec> levels;

        public event Action<LevelSpec> NextLevelSelected;

        private List<bool> _unlockedLevels = new List<bool>();
        private int _currentLevelIdx;

        private int CurrentLevelIdx
        {
            get => _currentLevelIdx;
            set => _currentLevelIdx = value % levels.Count;
        }

        [Inject] private ObjectManager _objectManager;
        [Inject] private DifficultyManager _difficultyManager;
        [Inject] private WinManager _winManager;

        private void Awake()
        {
            _winManager.LevelComplete += SaveProgress;
        }

        public void ContinueGame()
        {
            LoadProgress();
            InitLevel(false);
        }

        public void NewGame()
        {
            DeleteProgress();
            InitLevel(false);
        }

        public void NextLevel()
        {
            CurrentLevelIdx++;
            InitLevel(false);
        }

        public void PrevLevel()
        {
            CurrentLevelIdx--;
            InitLevel(true);
        }

        private void InitLevel(bool prev)
        {
            var level = levels[CurrentLevelIdx];
            _difficultyManager.DifficultyLevel = level.DifficultyLevel;
            _objectManager.Init(level.MeshObjects, prev);
            NextLevelSelected?.Invoke(level);
        }

        private void SaveProgress()
        {
            PlayerPrefs.SetString(nameof(_unlockedLevels),
                Convert.ToBase64String(
                    SerializationUtility.SerializeValue(_unlockedLevels, DataFormat.JSON)));
            PlayerPrefs.SetInt(nameof(CurrentLevelIdx), CurrentLevelIdx);
        }
        
        private void LoadProgress()
        {
            _unlockedLevels = SerializationUtility.DeserializeValue<List<bool>>(
                Convert.FromBase64String(
                    PlayerPrefs.GetString(nameof(_unlockedLevels))), DataFormat.JSON);
            CurrentLevelIdx = PlayerPrefs.GetInt(nameof(CurrentLevelIdx));
        }

        private void DeleteProgress()
        {
            PlayerPrefs.DeleteKey(nameof(CurrentLevelIdx));
            PlayerPrefs.DeleteKey(nameof(_unlockedLevels));
        }
    }
}
