﻿using System.Collections.Generic;
using GameSettings;
using Object.ScriptableObjects;
using UnityEngine;

namespace Game
{
	[CreateAssetMenu(fileName = "Level", menuName = "Game/Level", order = 0)]
	public class LevelSpec : ScriptableObject
	{
		[SerializeField] private List<MeshObjectSpec> meshObjects;
		[SerializeField] private DifficultyLevel difficultyLevel = DifficultyLevel.Easy;
		[SerializeField] private string hint;

		public List<MeshObjectSpec> MeshObjects => meshObjects;
		public DifficultyLevel DifficultyLevel => difficultyLevel;
		public string Hint => hint;
	}
}