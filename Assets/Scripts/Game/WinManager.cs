﻿using System;
using System.Collections;
using Object;
using UnityEngine;
using Zenject;

namespace Game
{
	public class WinManager : MonoBehaviour
	{
		public event Action LevelComplete;

		[Inject] private ObjectManager _objectManager;
		[Inject] private LevelManager _levelManager;

		private void Awake()
		{
			_levelManager.NextLevelSelected += _ =>
			{
				StartCoroutine(CheckValidRoutine());
				IEnumerator CheckValidRoutine()
				{
					yield return new WaitUntil(() => _objectManager.AllObjectsAreValid());
					LevelComplete?.Invoke();
				}
			};
		}
	}
}