﻿using UnityEngine;

namespace GameSettings
{
	public class DifficultyManager : MonoBehaviour, IDifficultyProvider, IDifficultySetter
	{
		[SerializeField] private DifficultyLevel testDifficulty;
		public DifficultyLevel DifficultyLevel { get; set; }

		private void Awake()
		{
			DifficultyLevel = testDifficulty;
		}
	}
}