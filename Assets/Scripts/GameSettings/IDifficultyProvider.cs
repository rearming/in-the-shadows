﻿namespace GameSettings
{
	public interface IDifficultyProvider
	{
		DifficultyLevel DifficultyLevel { get; }
	}
}