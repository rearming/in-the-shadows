﻿namespace GameSettings
{
	public interface IDifficultySetter
	{
		DifficultyLevel DifficultyLevel { set; }
	}
}