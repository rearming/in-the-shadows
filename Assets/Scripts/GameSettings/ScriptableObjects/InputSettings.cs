﻿using UnityEngine;

namespace GameSettings.ScriptableObjects
{
	[CreateAssetMenu(fileName = "Input Settings", menuName = "Game Settings/Input", order = 0)]
	public class InputSettings : ScriptableObject
	{
		[SerializeField] private Vector2 rotationSpeed;
		[Range(0f, 1f)]
		[SerializeField] private float moveSpeed;

		public Vector2 RotationSpeed => rotationSpeed;
		public float MoveSpeed => moveSpeed;
	}
}