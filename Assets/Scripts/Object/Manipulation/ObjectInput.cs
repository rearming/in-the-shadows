﻿using System;
using Camera;
using Game;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Object.Manipulation
{
	public class ObjectInput : MonoBehaviour
	{
		public event Action TryMoveObject;
		public event Action TryRotateObject;
		public bool InteractionProcess { get; private set; }
		public bool InputBlocked { get; private set; }
		public bool FirstEvent { get; private set; }

		[Inject] private IObjectTracer _objectTracer;
		[Inject] private WinManager _winManager;
		[Inject] private LevelManager _levelManager;

		private void Start()
		{
			_winManager.LevelComplete += () => InputBlocked = true;
			_levelManager.NextLevelSelected += (_) => InputBlocked = false;
		}

		private void Update()
		{
			if (Input.GetMouseButtonDown((int) MouseButton.RightMouse) ||
			    Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
				Select();
			if (Input.GetMouseButtonUp((int) MouseButton.RightMouse) ||
			    Input.GetMouseButtonUp((int) MouseButton.LeftMouse))
				InteractionProcess = false;
			FirstEvent = false;
		}
		
		private void Select()
		{
			if (!_objectTracer.TraceObject(out var hit) || hit.collider.gameObject != gameObject)
				return;
			InteractionProcess = true;
			FirstEvent = true;
			
			if (Input.GetMouseButtonDown((int)MouseButton.RightMouse))
				TryRotateObject?.Invoke();
			if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse))
				TryMoveObject?.Invoke();
		}
	}
}