﻿using System;
using System.Collections;
using Camera;
using GameSettings;
using GameSettings.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace Object.Manipulation
{
	public class ObjectMover : MonoBehaviour
	{
		public Vector3 Position
		{
			get => transform.position;
			set => transform.position = value;
		}
		
		public Vector3 WantedPosition { get; private set; }

		private ObjectInput _objectInput;
		private Plane _movementPlane;

		[Inject] private IDifficultyProvider _difficultyProvider;
		[Inject] private InputSettings _inputSettings;
		[Inject] private ICameraProvider _cameraProvider;

		private void Awake()
		{
			_objectInput = GetComponent<ObjectInput>();
			_objectInput.TryMoveObject += () => StartCoroutine(HandleMovementInputRoutine());

			_movementPlane = new Plane(Vector3.back, transform.position);
		}

		private IEnumerator HandleMovementInputRoutine()
		{
			if (_difficultyProvider.DifficultyLevel < DifficultyLevel.Hard)
				yield break;
			
			while (_objectInput.InteractionProcess && !_objectInput.InputBlocked)
			{
				var ray = _cameraProvider.Camera.ScreenPointToRay(Input.mousePosition);
				if (!_movementPlane.Raycast(ray, out var distance))
					yield return null;
				WantedPosition = ray.GetPoint(distance);
				yield return null;
			}
		}

		private void Update()
		{
			DampMovement();
		}

		private void DampMovement()
		{
			Position = Vector3.Lerp(transform.position, WantedPosition, _inputSettings.MoveSpeed);
		}
	}
}