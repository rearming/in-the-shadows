﻿using System.Collections;
using GameSettings;
using Object.ScriptableObjects;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Object.Manipulation
{
	public class ObjectPermutator : MonoBehaviour
	{
		[SerializeField] private float duration = 3f;

		private ObjectValidator _validator;
		
		[Inject] private ObjectValidationSettings _objectValidationSettings;
		[Inject] private IDifficultyProvider _difficultyProvider;

		private void Awake()
		{
			_validator = GetComponent<ObjectValidator>();
		}

		public void Permute(bool animate)
		{
			if (animate)
				StartCoroutine(PermuteRoutine());
			else
				(transform.position, transform.rotation) = GetPermutation();

			IEnumerator PermuteRoutine()
			{
				var (permutedPosition, permutedRotation) = GetPermutation();
				var initialPosition = transform.position;
				var initialRotation = transform.rotation;
				for (var i = 0f; i <= duration; i += Time.deltaTime)
				{
					var t = i / duration;
					transform.position = Vector3.Lerp(initialPosition, permutedPosition, t);
					transform.rotation = Quaternion.Slerp(initialRotation, permutedRotation, t);
					yield return null;
				}
				transform.position = Vector3.Lerp(initialPosition, permutedPosition, 1f);
				transform.rotation = Quaternion.Slerp(initialRotation, permutedRotation, 1f);
			}
		}

		private (Vector3, Quaternion) GetPermutation()
		{
			var rotationPermutation = _validator.CorrectRotation.eulerAngles;
			var movementPermutation = _validator.CorrectPosition;
			
			if (_difficultyProvider.DifficultyLevel >= DifficultyLevel.Easy)
				rotationPermutation.y += Random.Range(-90f, 90f);
			if (_difficultyProvider.DifficultyLevel >= DifficultyLevel.Medium)
				rotationPermutation.x = Random.Range(-90f, 90f);
			if (_difficultyProvider.DifficultyLevel >= DifficultyLevel.Hard)
			{
				movementPermutation.x = Random.Range(-_objectValidationSettings.MinMaxPositionShift.x,
					_objectValidationSettings.MinMaxPositionShift.x);
				movementPermutation.y = Random.Range(-_objectValidationSettings.MinMaxPositionShift.y,
					_objectValidationSettings.MinMaxPositionShift.y);
			}
			
			return (movementPermutation, Quaternion.Euler(rotationPermutation));
		}
	}
}