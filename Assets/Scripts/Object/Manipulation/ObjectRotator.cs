﻿using System.Collections;
using Extensions;
using GameSettings;
using GameSettings.ScriptableObjects;
using Sirenix.Serialization;
using UnityEngine;
using Zenject;

namespace Object.Manipulation
{
	public class ObjectRotator : MonoBehaviour
	{
		[OdinSerialize]
		public Quaternion Rotation
		{
			get => transform.rotation;
			set => transform.rotation = value;
		}

		private ObjectInput _objectInput;
		
		[Inject] private IDifficultyProvider _difficultyProvider;
		[Inject] private InputSettings _inputSettings;

		private void Awake()
		{
			_objectInput = GetComponent<ObjectInput>();
			_objectInput.TryRotateObject += () => StartCoroutine(HandleRotationInputRoutine());
		}

		private IEnumerator HandleRotationInputRoutine()
		{
			var prevPos = Vector2.zero;

			while (_objectInput.InteractionProcess && !_objectInput.InputBlocked)
			{
				if (_objectInput.FirstEvent)
					prevPos = Input.mousePosition;
				var delta = prevPos - (Vector2)Input.mousePosition;
				Rotate(delta.Swap());
				prevPos = Input.mousePosition;
				yield return null;
			}
		}

		private void Rotate(Vector3 delta)
		{
			delta.Scale(_inputSettings.RotationSpeed);
			if (_difficultyProvider.DifficultyLevel == DifficultyLevel.Easy)
				delta.x = 0f;
			transform.Rotate(Vector3.up, delta.y, Space.World);
			transform.Rotate(Vector3.left, delta.x, Space.World);
		}
	}
}