using Object.Manipulation;
using Object.ScriptableObjects;
using UnityEngine;

namespace Object
{
	public class MeshObject : MonoBehaviour
	{
		public bool Initialized { get; private set; }
		public ObjectPermutator Permutator { get; private set; }
		public ObjectValidator Validator { get; private set; }
		public MeshObjectSpec Spec { get; private set; }

		private MeshFilter _meshFilter;
		private MeshRenderer _meshRenderer;

		private ObjectMoveSwitcher _moveSwitcher;

		private void Awake()
		{
			_meshFilter = GetComponent<MeshFilter>();
			_meshRenderer = GetComponent<MeshRenderer>();
			Permutator = GetComponent<ObjectPermutator>();
			Validator = GetComponent<ObjectValidator>();
			_moveSwitcher = GetComponent<ObjectMoveSwitcher>();
		}

		public MeshObject Init(MeshObjectSpec meshObjectSpec, Vector3 startPos)
		{
			Spec = meshObjectSpec;
			_meshFilter.mesh = Spec.Mesh;
			GetComponent<MeshCollider>().sharedMesh = Spec.Mesh;
			transform.localScale = Vector3.one * Spec.Scale;
			
			_moveSwitcher.MoveIntoScene(startPos);
			_moveSwitcher.MoveComplete += () => Initialized = true;
			
			return this;
		}

		public void Deinit(Vector3 endPos)
		{
			_moveSwitcher.RemoveFromScene(endPos);
		}
	}
}