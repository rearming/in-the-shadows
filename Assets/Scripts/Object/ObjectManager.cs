﻿using System;
using System.Collections.Generic;
using System.Linq;
using Object.ScriptableObjects;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Object
{
	public class ObjectManager : MonoBehaviour
	{
		[SerializeField] private Transform startAnchor;
		[SerializeField] private Transform endAnchor;
		
		private readonly List<MeshObject> _meshObjects = new List<MeshObject>();

		[Inject] private MeshObject _meshObjectPrefab;
		[Inject] private DiContainer _container;

		[Button]
		public void Init(List<MeshObjectSpec> meshObjectSpecs, bool asPrevious)
		{
			_meshObjects.ForEach(prevMeshObject =>
			{
				prevMeshObject.Deinit(asPrevious ? startAnchor.position : endAnchor.position);
			});

			_meshObjects.Clear();
			meshObjectSpecs.ForEach(spec =>
			{
				var meshObject = _container
					.InstantiatePrefabForComponent<MeshObject>(_meshObjectPrefab, transform)
					.Init(spec, asPrevious ? endAnchor.position : startAnchor.position);
				if (!asPrevious)
					meshObject.Permutator.Permute(false);
				_meshObjects.Add(meshObject);
			});
		}

		[Button]
		public bool AllObjectsAreValid()
		{
			return _meshObjects.All(mo => mo.Validator.IsValid && mo.Initialized);
		}
	}
}