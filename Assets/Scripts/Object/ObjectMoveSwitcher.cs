﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

namespace Object
{
	public class ObjectMoveSwitcher : MonoBehaviour
	{
		[SerializeField] private float moveDuration = 4f;

		public event Action MoveComplete;
		
		private MeshObject _meshObject;
		
		private void Awake()
		{
			_meshObject = GetComponent<MeshObject>();
		}

		public void MoveIntoScene(Vector3 startPos)
		{
			StartCoroutine(MoveIntoSceneRoutine());
			
			IEnumerator MoveIntoSceneRoutine()
			{
				yield return StartCoroutine(MoveObject(_meshObject.transform, startPos,
					_meshObject.transform.position, Easing.InOutSine));
				MoveComplete?.Invoke();
			}
		}

		public void RemoveFromScene(Vector3 endPos)
		{
			StartCoroutine(RemoveFromSceneRoutine());
			
			IEnumerator RemoveFromSceneRoutine()
			{
				yield return StartCoroutine(MoveObject(_meshObject.transform, _meshObject.transform.position,
					endPos, Easing.InOutSine)); // todo fix object flickering
				Destroy(_meshObject.gameObject);
			}
		}
		
		private IEnumerator MoveObject(Transform target, Vector3 startPos, Vector3 endPos, Func<float, float> easingFunc)
		{
			for (var i = 0f; i <= moveDuration; i += Time.deltaTime)
			{
				var t = i / moveDuration;
				target.position = Vector3.Lerp(startPos, endPos, easingFunc(t));
				yield return null;
			}
			target.position = Vector3.Lerp(startPos, endPos, easingFunc(1f));
		}
	}
}