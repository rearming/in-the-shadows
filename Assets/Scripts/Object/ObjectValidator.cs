﻿using System.Linq;
using Extensions;
using Object.Manipulation;
using Object.ScriptableObjects;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Zenject;

namespace Object
{
	public class ObjectValidator : MonoBehaviour
	{
		private MeshObject _meshObject;
		private ObjectRotator _rotator;
		private ObjectMover _mover;

		[Inject] private ObjectValidationSettings _validationSettings;

		private void Awake()
		{
			_meshObject = GetComponent<MeshObject>();
			_rotator = GetComponent<ObjectRotator>();
			_mover = GetComponent<ObjectMover>();
		}

		public bool IsValid
		{
			get
			{
				var axes = _meshObject.Spec.CheckedAxes;
				var selfRotation = GetCheckedRotation(_rotator.Rotation, axes);
				var rotationValid = _meshObject.Spec.CorrectRotations.Any(rotation =>
					Quaternion.Angle(selfRotation, GetCheckedRotation(rotation, axes)) <= _validationSettings.AngleTolerance);
				
				       (_mover.Position.x - _meshObject.Spec.CorrectPosition.x).Abs() <=
				       _validationSettings.PositionTolerance.x &&
				       (_mover.Position.y - _meshObject.Spec.CorrectPosition.y).Abs() <=
				       _validationSettings.PositionTolerance.y;
			}
		}

		private Quaternion GetCheckedRotation(Quaternion rawRotation, PrimitiveBoundsHandle.Axes axes)
		{
			return new Vector3(
					(axes & PrimitiveBoundsHandle.Axes.X) != 0 ? rawRotation.eulerAngles.x : 0, 
					(axes & PrimitiveBoundsHandle.Axes.Y) != 0 ? rawRotation.eulerAngles.y : 0, 
					(axes & PrimitiveBoundsHandle.Axes.Z) != 0 ? rawRotation.eulerAngles.z : 0)
				.ToQuaternion();
		}

		public Vector3 CorrectPosition => _meshObject.Spec.CorrectPosition;
		public Quaternion CorrectRotation => _meshObject.Spec.CorrectRotations.Random();
	}
}