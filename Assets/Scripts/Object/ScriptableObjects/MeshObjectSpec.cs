﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Zenject;

namespace Object.ScriptableObjects
{
	[CreateAssetMenu(fileName = "Mesh Object Spec", menuName = "Game/Mesh Object", order = 0)]
	public class MeshObjectSpec : SerializedScriptableObject
	{
		[SerializeField] private Mesh mesh;
		[SerializeField] private float scale;
		[SerializeField] private Vector3 correctPosition;

		[SerializeField] private List<Quaternion> correctRotations = new List<Quaternion>();
		[SerializeField] private PrimitiveBoundsHandle.Axes checkedAxes = PrimitiveBoundsHandle.Axes.All;

		public Mesh Mesh => mesh;
		public float Scale => scale;
		public IReadOnlyList<Quaternion> CorrectRotations => correctRotations;
		public Vector3 CorrectPosition => correctPosition;
		public PrimitiveBoundsHandle.Axes CheckedAxes => checkedAxes;

		[Inject] private DiContainer _container;
		
		[Button]
		private void AddCorrectRotation()
		{
			var obj = FindObjectsOfType<MeshObject>().FirstOrDefault(meshObject => meshObject.Spec == this);
			if (obj == null)
				return;
			Debug.Log($"Setting correct rotation [{obj.transform.rotation}] (euler [{obj.transform.localEulerAngles}]) for [{name}].");
			correctRotations.Add(obj.transform.rotation);
		}
	}
}