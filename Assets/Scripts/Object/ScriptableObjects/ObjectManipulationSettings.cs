﻿using UnityEngine;

namespace Object.ScriptableObjects
{
	[CreateAssetMenu(fileName = "Object Manipulation Settings", menuName = "Game Settings/Object Manipulation Settings", order = 0)]
	public class ObjectManipulationSettings : ScriptableObject
	{
		[SerializeField] private float rotationDamp = 0.2f;
		[SerializeField] private float movementDamp = 0.2f;
		
		public float RotationDamp => rotationDamp;
		public float MovementDamp => movementDamp;
	}
}