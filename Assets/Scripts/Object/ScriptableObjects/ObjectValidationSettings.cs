﻿using UnityEngine;

namespace Object.ScriptableObjects
{
	[CreateAssetMenu(fileName = "Object Validation Settings", menuName = "Game/Object Validation Settings", order = 0)]
	public class ObjectValidationSettings : ScriptableObject
	{
		[SerializeField] private Vector2 minMaxPositionShift;

		[SerializeField] private float angleTolerance;
		[SerializeField] private Vector2 positionTolerance;

		public Vector2 MinMaxPositionShift => minMaxPositionShift;
		public float AngleTolerance => angleTolerance;
		public Vector2 PositionTolerance => positionTolerance;
	}
}