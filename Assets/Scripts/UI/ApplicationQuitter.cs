﻿using UnityEngine;

namespace UI
{
	public class ApplicationQuitter : MonoBehaviour
	{
		public void Quit()
		{
			Application.Quit();
		}
	}
}