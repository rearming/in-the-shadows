﻿using UnityEngine;

namespace UI
{
	public class UICanvasGroupAlphaLerper : UILerperBase
	{
		private CanvasGroup _canvasGroup;
		
		private void Awake()
		{
			_canvasGroup = GetComponent<CanvasGroup>();
		}

		protected override void LerpBody(float t)
		{
			_canvasGroup.alpha = t;
		}
	}
}