﻿using System.Collections.Generic;
using System.Linq;
using Extensions;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class UIGraphicAlphaLerper : UILerperBase
	{
		[InlineButton(nameof(FindTargets))]
		[SerializeField] private List<Graphic> graphicTargets;

		protected override void LerpBody(float t)
		{
			graphicTargets.ForEach(target => target.color = target.color.SetA(t));
		}

		private void FindTargets()
		{
			graphicTargets = GetComponentsInChildren<Graphic>().ToList();
		}
	}
}