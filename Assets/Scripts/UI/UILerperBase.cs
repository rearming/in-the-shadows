﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace UI
{
	public abstract class UILerperBase : MonoBehaviour
	{
		[SerializeField] protected float duration = 1f;
		[SerializeField] protected EasingProvider.EasingType easingType = EasingProvider.EasingType.Linear;

		public event Action<bool> LerpStart;
		public event Action<bool> LerpEnd;
		
		[Button]
		public void Lerp(bool backwards = false, EasingProvider.EasingType? easingOverride = null)
		{
			var easingFunc = EasingProvider.GetEasing(easingOverride ?? easingType);
			StartCoroutine(LerpRoutine());
			
			IEnumerator LerpRoutine()
			{
				LerpStart?.Invoke(!backwards);
				for (var i = 0f; i <= duration; i += Time.deltaTime)
				{
					var t = i / duration;
					LerpBody(easingFunc.Invoke(backwards ? 1f - t : t));
					yield return null;
				}
				LerpBody(easingFunc.Invoke(backwards ? 0f : 1f));
				LerpEnd?.Invoke(!backwards);
			}
		}
		
		protected abstract void LerpBody(float t);
	}
}