﻿using Game;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI
{
	public class UIManager : MonoBehaviour
	{
		[SerializeField] private UISwitchableElement mainMenuButtons;

		[SerializeField] private UISwitchableButton continueButton;
		[SerializeField] private Button newGameButton;
		[SerializeField] private Button cheatGameButton;

		[SerializeField] private UISwitchableButton nextLevelButton;
		[SerializeField] private UISwitchableButton prevLevelButton;
		[SerializeField] private UISwitchableButton backToMainMenuButton;
		[SerializeField] private UISwitchableButton resetObjectButton;

		[SerializeField] private UISwitchableElement levelHint;
		private Text _levelHintText;

		[SerializeField] private UISwitchableElement levelComplete;

		[Inject] private LevelManager _levelManager;
		[Inject] private WinManager _winManager;

		private void Awake()
		{
			_levelHintText = levelHint.GetComponentInChildren<Text>();
			
			newGameButton.onClick.AddListener(() =>
			{
				mainMenuButtons.Switch(false);
				backToMainMenuButton.Switch(true);
				_levelManager.NewGame();
			});

			_winManager.LevelComplete += () =>
			{
				nextLevelButton.Switch(true);
				levelHint.Switch(false);
				levelComplete.Switch(true);
			};
			
			nextLevelButton.Clicked += () =>
			{
				_levelManager.NextLevel();
				nextLevelButton.Switch(false);
			};

			_levelManager.NextLevelSelected += levelSpec =>
			{
				_levelHintText.text = levelSpec.Hint;
				levelHint.Switch(true);
				levelComplete.Switch(false);
			};

			backToMainMenuButton.Clicked += () =>
			{
				mainMenuButtons.Switch(true);
				nextLevelButton.Switch(false);
				prevLevelButton.Switch(false);
				resetObjectButton.Switch(false);
				backToMainMenuButton.Switch(false);
				levelHint.Switch(false);
			};

			resetObjectButton.Clicked += () =>
			{
				resetObjectButton.Switch(false);
				// todo logic here
			};
			// todo actions on prevLevel, resetObject, cheatGame
		}
	}
}