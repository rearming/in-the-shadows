﻿using UnityEngine;

namespace UI
{
	public class UIPositionLerper : UILerperBase
	{
		[SerializeField] private Transform startAnchor;
		[SerializeField] private Transform endAnchor;

		protected override void LerpBody(float t) =>
			transform.position = Vector3.Lerp(startAnchor.position, endAnchor.position, t);
	}
}