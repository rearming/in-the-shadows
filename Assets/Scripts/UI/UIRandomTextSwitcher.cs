﻿using System;
using System.Collections.Generic;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	[RequireComponent(typeof(UISwitchableElement))]
	public class UIRandomTextSwitcher : MonoBehaviour
	{
		[SerializeField] private List<string> strings;

		private UISwitchableElement _switchableElement;
		private Text _text;

		private void Awake()
		{
			_switchableElement = GetComponent<UISwitchableElement>();
			_text = GetComponentInChildren<Text>();
			
			_text.text = strings.Random();
			_switchableElement.Disabled += () =>
			{
				string newText;
				do
					newText = strings.Random();
				while (_text.text == newText);
				_text.text = newText;
			};
		}
	}
}