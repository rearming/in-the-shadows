﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	[RequireComponent(typeof(Button))]
	public class UISwitchableButton : UISwitchableElement
	{
		public event Action Clicked;
		
		private Button _button;

		protected override void Awake()
		{
			base.Awake();

			_button = GetComponent<Button>();
			_button.onClick.AddListener(() => Clicked?.Invoke());
		}
	}
}