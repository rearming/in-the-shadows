﻿using System;
using Extensions;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace UI
{
	[RequireComponent(typeof(UILerperBase))]
	public class UISwitchableElement : SerializedMonoBehaviour
	{
		[OdinSerialize] public bool IsEnabled { get; private set; } = true;
		
		public event Action Enabled;
		public event Action Disabled;

		private int _cachedLayer;
		private UILerperBase _lerper;

		protected virtual void Awake()
		{
			_cachedLayer = gameObject.layer;

			_lerper = GetComponent<UILerperBase>();
			_lerper.LerpEnd += elementEnabled =>
			{
				if (elementEnabled)
				{
					gameObject.layer = _cachedLayer;
					Enabled?.Invoke();
				}
				else
				{
					gameObject.DisableRaycast();
					Disabled?.Invoke();
				}
			};
		}

		public void Switch(bool enable)
		{
			if (IsEnabled == enable)
				return;
			IsEnabled = enable;
			_lerper.Lerp(!enable);
		}
	}
}