﻿using System;
using UnityEngine.UIElements.Experimental;

namespace Utils
{
	public static class EasingProvider
	{
		public enum EasingType
		{
			Step,
			Linear,
			InSine,
			OutSine,
			InOutSine,
			InQuad,
			OutQuad,
			InOutQuad,
			InCubic,
			OutCubic,
			InOutCubic,
			InBounce,
			OutBounce,
			InOutBounce,
			InElastic,
			OutElastic,
			InOutElastic,
			InBack,
			OutBack,
			InOutBack,
			InCirc,
			OutCirc,
			InOutCirc,
		}

		public static Func<float, float> GetEasing(EasingType type)
		{
			return type switch
			{
				EasingType.Step => Easing.Step,
				EasingType.Linear => Easing.Linear,
				EasingType.InSine => Easing.InSine,
				EasingType.OutSine => Easing.OutSine,
				EasingType.InOutSine => Easing.InOutSine,
				EasingType.InQuad => Easing.InQuad,
				EasingType.OutQuad => Easing.OutQuad,
				EasingType.InOutQuad => Easing.InOutQuad,
				EasingType.InCubic => Easing.InCubic,
				EasingType.OutCubic => Easing.OutCubic,
				EasingType.InOutCubic => Easing.InOutCubic,
				EasingType.InBounce => Easing.InBounce,
				EasingType.OutBounce => Easing.OutBounce,
				EasingType.InOutBounce => Easing.InOutBounce,
				EasingType.InElastic => Easing.InElastic,
				EasingType.OutElastic => Easing.OutElastic,
				EasingType.InOutElastic => Easing.InOutElastic,
				EasingType.InBack => Easing.InBack,
				EasingType.OutBack => Easing.OutBack,
				EasingType.InOutBack => Easing.InOutBack,
				EasingType.InCirc => Easing.InCirc,
				EasingType.OutCirc => Easing.OutCirc,
				EasingType.InOutCirc => Easing.InOutCirc,
				_ => Easing.Linear
			};
		}
	}
}